import React from 'react';
import { shallow } from 'enzyme';

import { ExpensesSummary } from '../../components/ExpensesSummary';

let wrapper;

beforeEach(() => {
  wrapper = shallow(<ExpensesSummary/>);
});

test('should show the total amount for just 1 expense', () => {
  wrapper.setProps({
    expenseCount: 1,
    expenseTotal: 9434
  });
  expect(wrapper).toMatchSnapshot();

});

test('should show the total amount for more than 1 expense', () => {
  wrapper.setProps({
    expenseCount: 2,
    expenseTotal: 9434
  });
  expect(wrapper).toMatchSnapshot();
});
