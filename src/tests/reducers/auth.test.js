import authReducer from '../../reducers/auth';

test('should setup default auth values', () => {
  const state = authReducer(undefined, {type: '@@INIT'});
  expect(state).toEqual({});
});

test('should set uid on login', () => {
  const action = {
    type: 'LOGIN',
    uid: 'skljhflkdsjhflkah'
  };

  const state = authReducer({}, action);
  expect(state.uid).toBe(action.uid);
});

test('should unset uid on logout', () => {
  const action = {
    type: 'LOGOUT'
  }
  const state = authReducer({uid: 'anything'}, action);
  expect(state).toEqual({});
});
