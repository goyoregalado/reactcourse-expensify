import React from 'react';
import { connect } from 'react-redux';

import ExpenseListItem from './ExpenseListItem';
import selectExpenses from '../selectors/expenses';

// To test this component with enzyme we don't want to use
// the "connected" version of the component, we want to
// pass in custom props not the ones from the store.

export const ExpenseList = (props) => (
  <div className="content-container">
    <div className="list-header">
      <div className="show-for-mobile">Expenses</div>
      <div className="show-for-desktop">Expense</div>
      <div className="show-for-desktop">Amount</div>
    </div>
    <div className="list-body">
      {
        props.expenses.length === 0 ? (
          <div className="list-item list-item--message">
            <span>No expenses</span>  
          </div>
        ) : (
          props.expenses.map((expense) => (
            <ExpenseListItem key={ expense.id } { ...expense }/>
          ))
        )
      }
    </div>

  </div>
);


/*
To understand whats going on under the hood
const ConnectedExpenseList = connect((state) => {
  return {
    expenses: state.expenses
  }
})(ExpenseList);

export default ConnectedExpenseList;

But the most common pattern is the next code
*/

const mapStateToProps = (state) => {
  return {
    expenses: selectExpenses(state.expenses, state.filters)
  }
};

export default connect(mapStateToProps)(ExpenseList)