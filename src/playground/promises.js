const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve({
      name: 'Andrew',
      age: 26
    });
    // There should be just one resolve per promise.
    //resolve('This is my other resolved data');
    //reject('Something went wrong!');
  }, 5000);
});


console.log('Before');

promise.then((data) => {
  console.log('1', data);
  return 'some data'
}).then((str) => {
  console.log('does this run???', str);
}).then(() => {
  // If there is a return, then the next then function will fire 
  // As the resolve method of this promise.
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        name: 'Andrew',
        age: 26
      });
      // There should be just one resolve per promise.
      //resolve('This is my other resolved data');
      //reject('Something went wrong!');
    }, 5000);
  });
}).then(() => {
  console.log('This is my other promise');

}).catch((error) => {
  console.log('Error: ', error);
});

// You can pass the catch code as the second argument of then.
promise.then((data) => {
  console.log('2', data);
}, (error) => {
  console.log('2 Error: ', error);
});

console.log('After');