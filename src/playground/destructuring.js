console.log('destructuring');

// Object destructuring.

/* const person = {
  name: 'Andrew',
  age: 27,
  location: {
    city: 'Philadelphia',
    temp: 92
  }
};

//const name = person.name;
//const age = person.age;
const {name, age} = person;

console.log(`${name} is ${age}`);

if (person.location.city && person.location.temp) {
  console.log(`It's ${person.location.temp} in ${person.location.city}`);
}

const { city, temp } = person.location;
if (city && temp) {
  console.log(`It's ${temp} in ${city}`);
}

// You can rename variables
const { city2, temp: temperature} = person.location;
if (city2 && temperature) {
  console.log(`It's ${temp} in ${city2}`);
}

// Setting default values.
// This person haven't name.
const person2 = {
  age: 27,
  location: {
    city: 'Philadelphia',
    temp: 92
  }
};

const { name2 = 'Anonymous', age: age2} = person2;
console.log(`${name2} is ${age2}`);

// You can rename and set default
const { name2: firstName = 'Anonymous', age: age3} = person2;
console.log(`${firstName} is ${age3}`); */

/* const book = {
  title: 'Ego is the enemy',
  author: 'Ryan Holiday',
  publisher: {
    name: 'Penguin'
  }
};

const {name: publisherName = 'Self-published'} = book.publisher;


console.log(publisherName); */


// Array destructuring
const address = ['1299 S Juniper Street', 'Philadelphia', 'Pennsylvania', '19147'];

// This is not clear.
console.log(`You are in ${address[1]} ${address[2]}.`)

// For array destructuring we use brackets not curly braces
// this matches by position.
// If you don't want to create variables for some positions you can leave blank 
// it's space.
const [, , state] = address;
console.log(`You are in ${state}.`)

// You can rename and set defaults on the fly.
const [, , newState] = address;
console.log(`You are in ${newState}.`)

const address2 = []
const [ , , state2 = 'New York'] = address2;
console.log(`You are in ${state2}.`)


const item = ['Coffe (hot)', '$2.00', '$2.50', '$2.75'];
const [productName, , price] = item;

console.log(`A medium ${productName} costs ${price}`)
