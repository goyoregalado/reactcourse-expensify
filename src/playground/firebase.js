import moment from 'moment';


/*
We should convert the output of firebase to an array.
So using, foreach method and iterating over every single child element
we can obtain the key using childSnapshot.key property and configure 
an feasible object.

database.ref('expenses')
  .once('value')
  .then((snapshot) => {
    const expenses = [];

    snapshot.forEach((childSnapshot) => {
      expenses.push({
        id: childSnapshot.key,
        ...childSnapshot.val()
      });
    });
    console.log(expenses);
  });*/

/*const onExpenseChange = database.ref('expenses').on('value', (snapshot) => {
  const expenses = [];
  snapshot.forEach((childSnapshot) => {
    expenses.push({
      id: childSnapshot.key,
      ...childSnapshot.val()
    });
  });
  console.log(expenses);
});*/

// We can listen to different events.
database.ref('expenses').on('child_removed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
});

database.ref('expenses').on('child_changed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
});

database.ref('expenses').on('child_added', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
})


database.ref('expenses').push({
  description: 'Mortage',
  amount: 37000,
  note: 'This is my home\'s monthly price',
  createdAt: moment().format('X')
});



// Firebase doesn't support arrays, it will convert the next array into 
// a list of objects which root element will be the array index.
/*const notes = [{
  id: '12',
  title: 'First note',
  body: 'This is my note'
},
{
  id: '1werw2',
  title: 'Another note',
  body: 'This is your note'
},
];*/

// If we pass such an structure like this firebase will use
// The note's object properties names as id's.
/*const firebaseNotes = {
  notes: {
    wer12: {
      title: 'First note!',
      body: 'This is my note'
    },
    werw2: {
      title: 'Another note',
      body: 'This is your note'
    }
  }
};*/

//database.ref('notes').set(notes);

// Using push firebase will generate random id's for 
// every object that we add to the reference.
/*database.ref('notes').push({
  title: 'Other thing',
  body: 'Go for a swim'
});*/

/*database.ref('notes/-LXIORbf8_vJLjSHx7ga').update({
  body: 'buy food'
});*/

/*const onValueChange = database.ref().on('value', (snapshot) => {
  const val = snapshot.val();
  console.log(`${val.name} is a ${val.job.title} at ${val.job.company}`);
});*/


// If we want the server to notify us on changes

/* const onValueChange = database.ref().on('value', (snapshot) => {
  console.log(snapshot.val());
}, (e) => {
  console.log('Error with data fetching', e);
});

setTimeout(() => {
  database.ref('age').set(28);
}, 3500);

setTimeout(() => {
  database.ref('age').set(29);
}, 7000);

setTimeout(() => {
  database.ref().off('value', onValueChange);
}, 8000);

setTimeout(() => {
  database.ref('age').set(30);
}, 10500);*/

/*
Using once we will take the data just one time, if
it fails or if it goes well, it will run just once.
database.ref()
  .once('value')
  .then((snapshot) => {
    const val = snapshot.val();
    console.log(val);
  })
  .catch((e) => {
    console.log('Error fetching data: ', e);
  })
*/
/*database.ref().set({
  name: 'Andrew Mead',
  age: 26,
  stressLevel: 6,
  job: {
    title: 'Sofware developer',
    company: 'Google'
  },
  location: {
    city: 'Philadelphia',
    country: 'USA'
  }
}).then(() => {
  console.log('Data is saved!');
}).catch((error) => {
  console.log('This is failed', error);
});*/

//database.ref().set('This is my data');

/*database.ref('age').set({
  age: 29
});

database.ref('location/city').set('La Orotava');*/

/*database.ref('attributes').set({
  height: 183,
  weight: 844
}).then(() => {
  console.log('Attributes saved!');
}).catch((e) => {
  console.log('There was an error!', e);
});*/

/*database.ref('isSingle')
  .remove()
  .then(() => {
    console.log('Data was removed');
  })
  .catch((error) => {
    console.log('Did not remove the data: ', e);
  });*/

// Using set passing null is the same as using remove.
//database.ref('isSingle').set(null);

// This kind of update only updates correctly the top level reference.
// Nested objects are not going to be updated, they will experiment the
// same if their were setted.
/*database.ref().update({
  job: 'Manager',
  location: {
    city: 'Boston'
  }
});*/

// With this syntax you can achieve the objective but you should acces
// the properties of nested objects using a kind of path strings.
/*database.ref().update({
  job: 'Manager',
  'location/city': 'Boston'
});*/

/*database.ref().update({
  stressLevel: 9,
  'job/company': 'Amazon',
  'location/city': 'Seattle'
});*/