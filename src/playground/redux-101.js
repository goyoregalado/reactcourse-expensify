import { createStore } from 'redux';


// We can destructure the arguments that we pass to a function.
const add = ({ a, b }, c) => {
  return a + b + c;
}

console.log(add({ a:1, b:12 }, 11));

// Action generators: Functions that return action objects.
// We deestructure the object and asign an default value.
// As the key in the object of the action generator is the same
// as the one in the deestructured object we just include the key
// and not the key/value pair.
const incrementCount = ({incrementBy = 1} = {}) => ({
  type: 'INCREMENT',
  incrementBy
});

const decrementCount = ({decrementBy = 1} = {}) => ({
  type: 'DECREMENT',
  decrementBy
})

const setCount = ({count})  => ({
  type: 'SET',
  count
});

const resetCount = () => ({
  type: 'RESET'
});


// Reducers
// 1.- Reducers are pure functions its output depends on the input and
// for the same input it will produce the same output and not rely in 
// values outside of the function scope.
// 2.- Never change state or action.

const countReducer = (state = { count: 0 }, action) => {

  switch (action.type) {
    case 'INCREMENT':
      const incrementBy = typeof action.incrementBy === 'number' ? action.incrementBy : 1;
      return {
        count: state.count + action.incrementBy
      };
    case 'DECREMENT':
      const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1;
      return {
        count: state.count - decrementBy
      };
    case 'RESET':
      return {
        count: 0
      };
    case 'SET':
      return {
        count: action.count
      }
    default:
      return state;
  }
};

const store = createStore(countReducer);


/* There we configure the create store with the reducer 
defined directly in the arrow function that is passed as 
an argument.
This is not an scalable way of doing things. 

const store = createStore((state = { count: 0 }, action) => {

  switch (action.type) {
    case 'INCREMENT':
      const incrementBy = typeof action.incrementBy === 'number' ? action.incrementBy : 1;
      return {
        count: state.count + action.incrementBy
      };
    case 'DECREMENT':
      const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1;
      return {
        count: state.count - decrementBy
      };
    case 'RESET':
      return {
        count: 0
      };
    case 'SET':
      return {
        count: action.count
      }
    default:
      return state;
  }
}); */

// Subscribe gets fired everytime the store changes.
// The return value of subscribe its the unsubscribe function.
const unsubscribe = store.subscribe(() => {
  console.log(store.getState());
});


// An action is an object that gets sent to the store.
// Examples of actions
// Increment, Decrement, Reset.

// Increment the count action.
// Dispatch sends this action to the store.
store.dispatch(incrementCount({ incrementBy: 5 }));

//unsubscribe();



// Decrement action
store.dispatch(decrementCount({}));

// Decrement action
store.dispatch(decrementCount({ decrementBy: 15 }));

store.dispatch(resetCount());

store.dispatch(setCount({count: 101}));


