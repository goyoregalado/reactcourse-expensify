export default (expenses) => {
  return expenses.reduce((acc, expense) => {
    return acc + expense.amount;
  }, 0);
};


// Andrew's proposal looks like this:
/*
export default (expenses) => {
  if (expenses.length === 0) {
    return 0;
  } else {
    return expenses
      .map((expense) => expense.amount)
      .reduce((sum, expense) => sum + value, 0);
  }
}

I don't understand why this is better.
*/
