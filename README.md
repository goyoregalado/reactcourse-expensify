--- Indecision App ---

These are the materials and my implementation of the applications proposed into Andrew Mead's React course (A really awesome course)

-- Git refresher --
git init - Create a new git repo
git status - View the changes to your project code
git add - Add files to your staging area.
git commit - Create a new commit with files from staging area.
git log - View recent commits